package org.innovatis.inMigrate.inMigrateExtension.config;

import org.innovatis.inMigrate.inInterfaces.InTransformer;
import org.innovatis.inMigrate.inMigrateExtension.transformer.AlfrescoDataTransformerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExtensionConfig {
	
	@Bean
	public InTransformer alfrescoDataTransformerImpl() {
		AlfrescoDataTransformerImpl alfrescoProcessor = new AlfrescoDataTransformerImpl();
		return alfrescoProcessor;
	}
}
